﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace AB02
{
    public partial class frmPingPong : Form
    {
        // Variblen festlegen
        private bool _godown, _goleft;
        public static int points;

        public frmPingPong()
        {
            InitializeComponent();
        }
        private void pnlSpiel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void picBall_Click(object sender, EventArgs e)
        {

        }

        private void btnSpielstarten_Click(object sender, EventArgs e)
        {
            // Intervaltimer aktivieren
            Spieltimer.Enabled = true;
        }

        private void Spieltimer_Tick(object sender, EventArgs e)
        {
            // Hitbox detection
            lblTime.Text = Convert.ToString(Spieltimer.Interval);
            if (picBall.Location.X + picBall.Width >= panel1.Location.X)
            {
                // entsprechende bool auf den richtigen Wert setzten
                _goleft = true;

                if (picBall.Location.Y - 35 <= panel1.Location.Y && picBall.Location.Y + 35 >= panel1.Location.Y)
                {
                    points += 10;
                    textBox1.Text = Convert.ToString(points);
                    if (points % 50 == 0 && points > 0)
                    {
                        Spieltimer.Interval = Spieltimer.Interval - 19;
                       
                        switch (RNG.random())
                        {
                            //ändern der Farbe des Balles
                            case 0:
                                picBall.BackColor = Color.LightYellow;
                                break;
                            case 1:
                                picBall.BackColor = Color.Yellow;
                                break;
                            case 2:
                                picBall.BackColor = Color.Orange;
                                break;
                            case 3:
                                picBall.BackColor = Color.Red;
                                break;
                        }
                        if (Spieltimer.Interval == 1)
                        {
                            // Wenn der Interval zu schnell ist hört es auf
                            Spieltimer.Enabled = false;
                            frmGameOver frm = new frmGameOver();
                            frm.Show();
                        }
                    }
                }
                else
                {
                    //öffnen des GameoverFrames
                    frmGameOver frm = new frmGameOver();
                    frm.punkte2 = "" + points;
                    frm.Show();
                    Spieltimer.Enabled = false;
                    picBall.Location = new Point(100, 150);
                }
                
             
            }

            if (picBall.Location.X + picBall.Width >= pnlSpiel.Width)
            {
                //Richtungsbool ==> position prüfen des Balles
                _goleft = true;
            }



            else if (picBall.Location.X <= pnlSpiel.Location.X)
            {
                //Richtungsbool ==> position prüfen des Balles
                _goleft = false;
            }

                if (picBall.Location.Y + picBall.Height >= pnlSpiel.Height)
                {
                    _godown = false;
                }

                else if (picBall.Location.Y <= pnlSpiel.Location.Y)
                {
                    _godown = true;
                }

            if (Spieltimer.Enabled)
                {
                    if (_goleft)
                    {
                        int newx = picBall.Location.X - 5;
                        picBall.Location = new Point(newx, picBall.Location.Y);
                    }

                    else if (!_goleft)
                    {
                        int newx = picBall.Location.X + 5;
                        picBall.Location = new Point(newx, picBall.Location.Y);
                    }

                    if (_godown)
                    {
                        int newy = picBall.Location.Y + 5;
                        picBall.Location = new Point(picBall.Location.X, newy);
                    }

                   else if (!_godown)
                    {
                        int newy = picBall.Location.Y - 5;
                        picBall.Location = new Point(picBall.Location.X, newy);
                    }
            }
        }

        private void lblTime_Click(object sender, EventArgs e)
        {
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            //Schläger Position
                panel1.Location = new Point(panel1.Location.X, pnlSpiel.Location.Y + pnlSpiel.Height / 100 * vScrollBar1.Value);
        }

        private void frmPingPong_Load(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            //Button im Frame zur bewegung des Balles/ Ballsteuerung
            picBall.Location = new Point(picBall.Location.X, picBall.Location.Y - 25);
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            //Button im Frame zur bewegung des Balles/ Ballsteuerung
            picBall.Location = new Point(picBall.Location.X + 25, picBall.Location.Y);
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            //Button im Frame zur bewegung des Balles/ Ballsteuerung
            picBall.Location = new Point(picBall.Location.X, picBall.Location.Y + 25);
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            //Button im Frame zur bewegung des Balles/ Ballsteuerung
            picBall.Location = new Point(picBall.Location.X - 25, picBall.Location.Y);
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void grpSteuerung_Enter(object sender, EventArgs e)
        {

        }

        private void btnBallsteuerung_CheckedChanged(object sender, EventArgs e)
        {
            //Radiobutton auswahl welche Setuerung man will und die Freischaltung der Jeweiligen Steuerung
            btnRight.Enabled = true;
            btnLeft.Enabled = true;
            btnUp.Enabled = true;
            btnDown.Enabled = true;
            vScrollBar1.Enabled = false;

        }

        private void rbtnSchlägersteuerung_CheckedChanged(object sender, EventArgs e)
        {
            //Radiobutton auswahl welche Setuerung man will und die Freischaltung der Jeweiligen Steuerung
            vScrollBar1.Enabled = true;
            btnRight.Enabled = false;
            btnLeft.Enabled = false;
            btnUp.Enabled = false;
            btnDown.Enabled = false;
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            //Richtungswechsel des Balles per Tasteneingabe/ Userinput
            if(keyData == Keys.V)
            {
                if (_godown)
                    _godown = false;
                else _godown = true;
            }
            if (keyData == Keys.H)
            {
                if (_goleft)
                    _goleft = false;
                else _goleft = true;
            }
            if (keyData == Keys.P)
            {
                Spieltimer.Enabled = false;
            }
            if (keyData == Keys.S)
            {
                Spieltimer.Enabled = true;
            }
            return base.ProcessDialogKey(keyData);
        }
    }

    public partial class RNG : Form 
    {
        public static int random()
        {
            //Macht eine Random Zahl zwischen 0 und 3 
            Random i = new Random();
            return i.Next(0, 3);
        }
    }
}


