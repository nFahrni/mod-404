﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AB01
{
    public partial class frmTaschenrechner : Form
    {
        

        public frmTaschenrechner()
        {
            InitializeComponent();
        }

        private void frmTaschenrechner_Load(object sender, EventArgs e)
        {

        }

        private void btnAddition_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text); 
           
            lblErgebnis.Text = Convert.ToString(Rechenoperationen.Addition(zahl1, zahl2));
            
            lblOperator.Text = "+";
        }

        private void btnSubtraktion_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);       
            lblErgebnis.Text = Convert.ToString(Rechenoperationen.Subtraktion(zahl1, zahl2));

            lblOperator.Text = "-";
        }





        private void btnMultiplikation_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            lblErgebnis.Text = Convert.ToString(Rechenoperationen.Multiplikation(zahl1, zahl2));

            lblOperator.Text = "*";
        }

        private void btnMittelwert_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            lblErgebnis.Text = Convert.ToString(Rechenoperationen.Mittelwert(zahl1, zahl2));
            
            lblOperator.Text = "|";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);   
            lblErgebnis.Text = Convert.ToString(Rechenoperationen.Division(zahl1, zahl2));

            lblOperator.Text = "/";
        }

        private void btnPotenz_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);
            
            lblErgebnis.Text = Convert.ToString(Rechenoperationen.Potenzierung(zahl1, zahl2));

            lblOperator.Text = "^";
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Maximum_Click(object sender, EventArgs e)
        {
            double zahl1 = Convert.ToDouble(txtOperand1.Text);
            double zahl2 = Convert.ToDouble(txtOperand2.Text);

            lblErgebnis.Text = Convert.ToString(Rechenoperationen.Maximum(zahl1, zahl2));

            lblOperator.Text = "^";
        }
    }
}
